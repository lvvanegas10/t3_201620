package parqueadero;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import Estructuras.Cola;
import Estructuras.Pila;


public class Central 
{
	/**
	 * Cola de carros en espera para ser estacionados
	 */
	private Cola<Carro> colaCarros;

	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */	
	private Pila<Carro>[] pilas;

	/**
	 * Pila de carros parqueadero temporal:
	 * Aca se estacionan los carros temporalmente cuando se quiere
	 * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
	 */
	private Pila<Carro> parqueaderoTemporal;

	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
	 */
	public Central ()
	{
		colaCarros= new Cola<Carro>();
		parqueaderoTemporal= new Pila<Carro>();
		pilas= new Pila[8];         
		for(int i=0; i<8;i++)
			pilas[i]= new Pila<Carro>();
	}

	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
	 * @param pColor color del vehiculo
	 * @param pMatricula matricula del vehiculo
	 * @param pNombreConductor nombre de quien conduce el vehiculo
	 */
	public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
	{
		colaCarros.encolar(new Carro(pColor, pMatricula, pNombreConductor));
	}    

	/**
	 * Parquea el siguiente carro en la cola de carros por parquear
	 * @return matricula del vehiculo parqueado y ubicaci�n
	 * @throws Exception
	 */
	public String parquearCarroEnCola() throws Exception
	{
		Carro primero = colaCarros.desencolar();
		return "MATRICULA: " + primero.darMatricula() + " "+  parquearCarro(primero);
	} 
	/**
	 * Saca del parqueadero el vehiculo de un cliente
	 * @param matricula del carro que se quiere sacar
	 * @return El monto de dinero que el cliente debe pagar
	 * @throws Exception si no encuentra el carro
	 */
	public double atenderClienteSale (String matricula) throws Exception
	{    	
		Carro buscado =sacarCarro(matricula);
		if( buscado ==null)
			throw new Exception("El veh�culo no est� registrado");
		
		return cobrarTarifa(buscado);
	}

	/**
	 * Busca un parqueadero co cupo dentro de los 8 existentes y parquea el carro
	 * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public String parquearCarro(Carro aParquear) throws Exception
	{
		boolean hay = false;
		for(int i=0;i<8 && !hay ;i++)
		{
			if(pilas[i].darTamano()<4)
			{
				hay= true;    			
				pilas[i].push(aParquear);
				return "PARQUEADERO " + (i+1); 
			}
		}     
		throw new Exception("No hay parqueaderos disponibles");    	
	}

	/**
	 * Itera sobre los 8 parqueaderos buscando uno con la placa ingresada
	 * @param matricula del vehiculo que se quiere sacar
	 * @return el carro buscado
	 */
	public Carro sacarCarro (String matricula)
	{
		Carro c = null;   
		for(int i=0; i<8 && c==null;i++)
		{
			Iterator<Carro> it = pilas[i].iterator();
			while(it.hasNext())
			{
				Carro car= (Carro) it.next();
				Carro temp= pilas[i].pop();			
				if(car.darMatricula().equals(matricula))
					c= car;
				else
					parqueaderoTemporal.push(temp);
			}
			vaciarPT(i);
		}
		return c;
	}   	

	/**
	 * Vacia el parqueadero temporal de nuevo en el parqueadero i
	 * @param i parqueadero en el cual se pondr�n los autos.
	 */
	public void vaciarPT(int i)
	{
		Iterator<Carro> it = parqueaderoTemporal.iterator();
		while(it.hasNext())
		{
			Carro car= (Carro) it.next();						
			pilas[i].push(parqueaderoTemporal.pop());				
		}
	}
	/**
	 * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
	 * la tarifa es de $25 por minuto
	 * @param car recibe como parametro el carro que sale del parqueadero
	 * @return el valor que debe ser cobrado al cliente 
	 */
	public double cobrarTarifa (Carro car)
	{
		Date hora= new Date();
		return (hora.getTime() - car.darLlegada())* 25;	
	}
	/**
	 * Devuelve el arreglo de pilas
	 */
	public Pila<Carro>[] parqueaderos()
	{
		return pilas;
	}
	/**
	 * Devuelve el numero de veh�culos en Cola
	 */
	public int numeroCola()
	{
		return colaCarros.darTamano();
	}
}
