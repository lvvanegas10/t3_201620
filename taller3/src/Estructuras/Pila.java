package Estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * Clase generica que representa una pila
 * @param Tipo de elempnto que se guarda en la cola
 */
public class Pila<T> {

	//-----------------------------------------
	// NODO: Reprenta un nodo de la lista
	//---------------------------------------
	/**
	 * Atributo del primer  elemto de la pila
	 */
	private Nodo cabeza = null;
	/**
	 * Tama�o de la pila
	 */
	private int tamano;

	/**
	 * Clase que representa un nodo
	*/
	private class Nodo
	{
		T item;
		Nodo next;
	}

	/**
	 * Constructor de la pila
	 */
	public Pila()
	{
		cabeza= null;
		tamano=0;
	}
	//-----------------------------------------
	// M�todos
	//---------------------------------------
	/**
	 * Si est� vacia la cola
	 * @return cabeza Promer elemento
	 */
	public boolean estaVacio()
	{
		return cabeza==null;
	}
	/**
	 * Agrega un elemto a la cola	
	 * @param pItem elemto a agregar
	 */
	public void push(T item)
	{
		Nodo primero = cabeza;
		cabeza = new Nodo();
		cabeza.item = item;
		cabeza.next = primero;
		tamano++;
	}
	/**
	 * Elimina el primer elento de la cola
	 * @return item el primer elento de la cola
	 */
	public T pop() 
	{
		if(estaVacio())
			return null;
		T item = cabeza.item;
		cabeza = cabeza.next;
		tamano--;
		return item;
	}
	/**
	 * Devuelve el tama�o 
	 */
	public int darTamano()
	{
		return tamano;
	}

	/**
	 * Itera la pila LIFO
	 * @return
	 */
	public Iterator<T> iterator() 
	{ 
		return (Iterator<T>) new ListIterator(); 
	}
	
	private class ListIterator implements Iterator<T> 
	{
		private Nodo actual = cabeza;
		public boolean hasNext() 
		{ 
			return actual != null;     
		}

		public T next()
		{
			if (!hasNext()) 
				throw new NoSuchElementException();
			T item = actual.item;
			actual = actual.next; 
			return item;
		}
	}
}
