package Estructuras;

/**
 * Clase generica que representa una cola
 * @param Tipo de elempnto que se guarda en la cola
 */
public class Cola <T>{

	//-----------------------------------------
	// NODO: Reprenta un nodo de la lista
	//---------------------------------------
	/**
	 * Atributo del primer y ultimo  elemto de la cola
	 */
	private Nodo cabeza, cola;	
	/**
	 * Tama�o de la pila
	 */
	private int tamano;

	/**
	 * Constructor de un nodo
	 */
	private class Nodo
	{
		T item;
		Nodo next;
	}

	/**
	 * Constructor de una cola
	 */
	public Cola()
	{
		cabeza=null;
		cola= null;
		tamano=0;
	}
	//-----------------------------------------
	// M�todos
	//---------------------------------------
	/**
	 * Si est� vacia la cola
	 * @return cabeza Promer elemento
	 */
	public boolean estaVacio()
	{
		return cabeza ==null;
	}

	/**
	 * Agrega un elemto a la cola	
	 * @param pItem elemto a agregar
	 */
	public void encolar(T pItem)
	{
		Nodo ultimo= cola;
		cola = new Nodo();
		cola.item = pItem;
		cola.next = null;
		if(estaVacio())	
			cabeza= cola;
		else
			ultimo.next= cola;
		tamano++;
	}
	/**
	 * Elimina el primer elento de la cola
	 * @return item el primer elento de la cola
	 */
	public T desencolar() throws Exception
	{
		if(estaVacio())
			throw new Exception("Cola vacia");
		T item = cabeza.item;
		cabeza= cabeza.next;
		tamano--;
		if(estaVacio())
			cola= null;
		return item;
	}
	/**
	 * Devuelve el tama�o 
	 */
	public int darTamano()
	{
		return tamano;
	}
}
