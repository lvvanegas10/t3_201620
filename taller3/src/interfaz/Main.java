package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import Estructuras.Pila;
import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{
		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}

	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
				+ "1. Registrar Cliente \n"
				+ "2. Parquear Siguente Carro En Cola \n"
				+ "3. Atender Cliente Que Sale \n"
				+ "4. Ver Estado Parqueaderos \n"
				+ "5. Ver Carros En Cola \n"
				+ "6. Salir \n\n"
				+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			try{
				int op1 = Integer.parseInt(br.readLine());
				while ((op1>6 || op1<1))
				{
					System.out.println("\nERROR: Ingrese una opci�n valida\n");
					System.out.print(mensaje);
					op1 = Integer.parseInt(br.readLine());
				}

				switch(op1)
				{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
				}
			}
			catch (Exception e)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");				
			}
		}

	}

	/**
	 * M�todo para registrar a un nuevo cliente
	 * @throws Exception
	 */
	public void registrarCliente () throws Exception
	{
		System.out.println("** REGISTRAR CLIENTE **");
		System.out.println("Ingrese el color del veh�culo");
		String pColor= br.readLine();

		System.out.println("Ingrese la mar�cula del veh�culo");
		String pMatricula= br.readLine();

		System.out.println("Ingrese el nombre del conductor del veh�culo");
		String pNombre= br.readLine();

		central.registrarCliente(pColor, pMatricula, pNombre);	
		System.out.println("------------------------------");
		System.out.println(" CLIENTE REGISTRADO CON �XITO");
		System.out.println("-------Veh�culo en cola-------");
		System.out.println("------------------------------");

	}
	/**
	 * M�todo para parquear un carro que se encuentra en la cola
	 * @throws Exception
	 */
	public void parquearCarro() throws Exception
	{
		System.out.println("** PARQUEAR CARRO EN COLA **");
		try{
			System.out.println(central.parquearCarroEnCola());
			System.out.println("-----------------------------");
			System.out.println("VEH�CULO PARQUEADO CON �XITO");
			System.out.println("-----------------------------");
		}
		catch(Exception e)
		{
			System.out.println("-----------------------------");
			System.out.println("ERROR: " + e.getMessage());
			System.out.println("-----------------------------");
		}
	}
	/**
	 * M�todo para sacar un carro del parqueadero
	 * @throws Exception
	 */
	public void salidaCliente () throws Exception
	{
		System.out.println("** REGISTRAR SALIDA CLIENTE **");
		System.out.println("Ingrese la matr�cula del veh�culo");
		String matricula= br.readLine();
		try{
			System.out.println("----------------------");
			System.out.println("  OPERACI�N �XITOSA");
			System.out.println("----------------------");
			System.out.println("---------------------");
			System.out.println("---------COSTO-------");
			System.out.println("El valor a pagar es: ");
			System.out.println(central.atenderClienteSale(matricula));

		}
		catch(Exception e)
		{
			System.out.println("-----------------------------");
			System.out.println("ERROR: " + e.getMessage());
			System.out.println("-----------------------------");
		}

	}
	/**
	 * M�todo que permite visualizar graficaente el estado de los parqueaderos
	 * @throws Exception
	 */
	public void verEstadoParquederos() throws Exception
	{
		Pila<Carro>[] l= central.parqueaderos();
		System.out.println("** ESTADO PARQUEADEROS **");
		System.out.println("-----------------------------");
		for (int i=0; i<8;i++)
		{
			String msg = "P"+(i+1)+ "  OCUPADOS: " + l[i].darTamano()+ "|";
			for(int j=0; j<l[i].darTamano();j++)
				msg += "�";
			System.out.println(msg);
		}
		System.out.println("-----------------------------");
	}

	/**
	 * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
	 * @throws Exception
	 */
	public void verCarrosEnCola () throws Exception
	{
		System.out.println("** ESTADO COLA **");
		int taman = central.numeroCola();
		String msg ="";
		for (int i=0; i<taman;i++)
			msg+= " + ";
		System.out.println("-----------------------------");
		System.out.println("El tama�o de la cola es:" + taman);
		System.out.println("COLA | " + msg);
		System.out.println("-----------------------------");
	}

	/**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
